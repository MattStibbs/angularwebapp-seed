angular.module("homeApp")
    .factory("appRuntime", ["$location", function($location) {
        "use strict";
        return {
            useLiveData: function() {
                var host, port;
                if ($location.protocol() === "file") {
                    return false;
                }
                host = $location.host();
                if (host === "locahost" || host === "127.0.0.1" || host.match(/^local\.\*+/)) {
                    return false;
                }
                port = $location.port();
                if (port >= 60000 && port <= 69999) {
                    return false;
                }
                return true;
            }
        };
    }
]);
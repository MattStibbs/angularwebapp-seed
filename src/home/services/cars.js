angular.module("homeApp")
    .factory("carsService", ["$resource", "globalServiceUris", "appRuntime", "testCars", function($resource, globalServiceUris, appRuntime, testCars) {
        "use strict";
        return {
            getCars: function(callback) {
                var result, carDetailsResource;
                result = null;
                if (appRuntime.useLiveData()) {
                    carDetailsResource = $resource(globalServiceUris.carsUri);
                    result = carDetailsResource.query(function() {
                        callback(result);
                    });
                } else {
                    result = testCars.getCars();
                    callback(result);
                }
            },
            getCar: function(reg, callback) {
                var result, carDetailsResource;
                result = null;
                if (appRuntime.useLiveData()) {
                    carDetailsResource = $resource(globalServiceUris.carsUri);
                    result = carDetailsResource.get({
                        reg: reg
                    }, function() {
                        callback(result);
                    });
                } else {
                    result = testCars.getCar(reg);
                    callback(result);
                }
            }
        };
    }
]);
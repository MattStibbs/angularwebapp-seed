angular.module("homeApp")
    .controller("HomeController", ["$scope", "$stateParams", "carsService", function($scope, $stateParams, carsService) {
        "use strict";

        carsService.getCars(function(cars) {
            $scope.cars = cars;
        });
    }]);
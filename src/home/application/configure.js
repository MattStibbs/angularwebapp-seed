angular.module("homeApp")
    .config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider) {
        "use strict";

        $urlRouterProvider.otherwise("/");

        // set-up the states
        $stateProvider
            .state("home", {
                url: "/",
                templateUrl: "home/partials/home.html",
                controller: "HomeController"
            })
            .state("car", {
                url: "/car/:reg",
                templateUrl: "home/partials/car.html",
                controller: "CarsController"
            });
    }]);
/**
 * Created by levym on 01/05/2014.
 */
"use strict";

describe("appRuntime", function() {
    var mockLocationService, runtimeService;
    runtimeService = void 0;
    mockLocationService = jasmine.createSpyObj("$location", ["protocol", "host", "port"]);

    beforeEach(module("homeApp"));

    beforeEach(module(function($provide) {
        $provide.value("$location", mockLocationService);
    }));

    beforeEach(inject(function($injector) {
        runtimeService = $injector.get("appRuntime");
    }));

    describe("When using live data", function() {
        beforeEach(function() {
            mockLocationService.protocol.andCallFake(function() {
                return "http";
            });
            mockLocationService.host.andCallFake(function() {
                return "localhost";
            });
            mockLocationService.port.andCallFake(function() {
                return 8080;
            });
        });
        it("should return true when using live data", function() {
            expect(runtimeService.useLiveData()).toBeTruthy();
        });
    });

    describe("when in file mode", function() {
        beforeEach(function() {
            mockLocationService.protocol.andCallFake(function() {
                return "file";
            });
            mockLocationService.host.andCallFake(function() {
                return "localhost";
            });
            mockLocationService.port.andCallFake(function() {
                return 8080;
            });
        });
        it("should return false when using live data", function() {
            expect(runtimeService.useLiveData()).toBeFalsy();
        });
    });
});
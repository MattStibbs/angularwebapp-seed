/**
 * Created by levym on 01/05/2014.
 */
"use strict";

describe("carsService", function() {
    var $httpBackend, mockRuntimeService, reg, carId, carsService, testUri, testCars;
    carsService = void 0;
    carId = "8D5FEB39F77D4F3F";
    reg = "this-is-a-test-car";
    testUri = "http://localhost/api/cars";

    mockRuntimeService = jasmine.createSpyObj("runtimeService", ["useLiveData"]);
    $httpBackend = void 0;

    beforeEach(module("homeApp"));

    beforeEach(module(function($provide) {
        var globalServiceUris;
        $provide.value("appRuntime", mockRuntimeService);
        globalServiceUris = {
            carsUri: testUri + "/:reg"
        };
        $provide.value("globalServiceUris", globalServiceUris);
    }));

    beforeEach(inject(function($injector) {
        $httpBackend = $injector.get("$httpBackend");
        carsService = $injector.get("carsService");
        testCars = $injector.get("testCars");
    }));

    beforeEach(function() {
        this.addMatchers({
            toEqualData: function(expect) {
                return angular.equals(expect, this.actual);
            }
        });
    });
    describe("when in http mode", function() {
        beforeEach(function() {
            mockRuntimeService.useLiveData.andCallFake(function() {
                return true;
            });
        });
        afterEach(function() {
            $httpBackend.verifyNoOutstandingExpectation();
            $httpBackend.verifyNoOutstandingRequest();
        });
        it("should return car details from the server", function() {
            var carDetails;
            carDetails = {
                carId: "123Test",
                "reg": "X3C5Z6",
                "make": "Nibh Aliquam Foundation"
            };
            $httpBackend.expect("GET", testUri + "/" + reg).respond(carDetails);
            carsService.getCar(reg, function(serverCarDetail) {
                expect(serverCarDetail).toEqualData(carDetails);
            });
            $httpBackend.flush();
        });
        it("should return cars from the server", function() {
            var carsArray;
            carsArray = [
                {
                    carId: "12Test",
                    "reg": "W0H6A1",
                    make: "Test car 1"
                },
                {
                    carId: "123Test",
                    "reg": "B0D1O9",
                    make: "Test car 2"
                }
            ];
            $httpBackend.expect("GET", testUri).respond(carsArray);
            carsService.getCars(function(serverCars) {
                expect(serverCars).toEqualData(carsArray);
            });
            $httpBackend.flush();
        });
    });
    describe("when in file mode", function() {
        beforeEach(function() {
            mockRuntimeService.useLiveData.andCallFake(function() {
                return false;
            });
        });
        describe("if car reg is not recognised", function() {
            it("should return no results", function() {
                reg = "blah-blah-blah";
                carsService.getCar(reg, function(carDetail) {
                    expect(carDetail).toBeNull();
                });
            });
        });
        it("should return car details from a local resource", function() {
            var expectedCarDetail;
            expectedCarDetail = testCars.getCar(carId);
            carsService.getCar(reg, function(serverCarDetail) {
                expect(serverCarDetail).toEqualData(expectedCarDetail);
            });
        });
        it("should return cars from a local resource", function() {
            var expectedCars;
            expectedCars = testCars.getCars();
            carsService.getCars(function(serverCars) {
                expect(serverCars).toEqualData(expectedCars);
            });
        });
    });
});
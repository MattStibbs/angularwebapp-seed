/**
 * Created by levym on 01/05/2014.
 */
"use strict";

describe("Application", function() {
    var globalServiceUris, stateService;

    beforeEach(module("homeApp"));

    globalServiceUris = null;
    stateService = null;

    beforeEach(inject(function($injector) {
        globalServiceUris = $injector.get("globalServiceUris");
        stateService = $injector.get("$state");
    }));

    it("Should return the postsUri", function() {
        expect(globalServiceUris.carsUri).toEqual("http://localhost:3000/api/cars/:reg");
    });

    it("Should return the home route", function() {
        expect(stateService.get("home").controller).toEqual("HomeController");
        expect(stateService.get("home").url).toEqual("/");
    });

    it("Should return the cars route", function() {
        expect(stateService.get("car").controller).toEqual("CarsController");
        expect(stateService.get("car").url).toEqual("/car/:reg");
    });
});